# This class will manage time for Linux hosts

class profile::ntp (
  Array[String] $servers = [ 'time.server.com', ],
) {
  case $facts['kernel'] {
    'Linux': {
      class { 'ntp':
        servers => $servers,
      }
    }
    default: {
      fail("Class ${title} does not support ${facts['kernel']}.")
    }
  }
}
