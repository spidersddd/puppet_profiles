# This profile is a base of additional thing we want done on 
# the puppetmaster server.
# This profile requires https://forge.puppetlabs.com/npwalker/pe_metric_curl_cron_jobs


class profile::puppetmaster {
  include pe_metric_curl_cron_jobs
}
