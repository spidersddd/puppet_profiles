# This is the base profile supporting this site

class profile::base {
  case $facts['osfamily'] {
    'Redhat', 'Debian',: {
      include ::profile::ntp
    }
    'windows': {
      include ::profile::wsus_client
    }
    default: {
      fail("Class ${title} does not support ${facts['osfamily']}.")
    }
  }
}
