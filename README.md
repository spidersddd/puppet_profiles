## Rough instructions on puppet code edits

### Edit control-repo to add new forge module

- From a terminal on host with the repositories in $HOME/code/control-repo directory
    - Run `git pull origin production` this is going do download the latest code from the gitlab server in branch production
    - Edit Puppetfile (`vim Puppetfile`)
    - Add 'mod' line in the specific location of the file from the module page on the forge.puppet.com (example: `mod 'puppetlabs-ntp', '7.1.1'`)
    - Add the new version of the file to git with `git add Puppetfile`
    - Create a git commit (`git commit -m "<message about what was added>"`)
    - Push the code to gitlab (`git push`)
- From a terminal on the puppetmaster as root
    - Run `puppet code deploy production --wait`
- If the deploy completes with no errors the new module has been installed
    - This will only add a new module to the index of modules that can be used.
    
    
### Edit profile code

- From a terminal on host with the repositories in $HOME/code/profile
    - Run `git pull origin master` this is going do download the latest code from the gitlab server in branch production
    - Edit or create a pp file in the manifests directory
    - Add the file to git (`git add <file name>`)
    - Create a git commit (`git commit -m "<message about what was added>"`)
    - Push the code to gitlab (`git push`)
    - Identify commit hash for use in the control-repo/Puppetfile, run `git show --summary`
    
    
### Edit hieradata code 

- From a terminal on host with the repositories in $HOME/code/hieradata
    - Run `git pull origin master` this is going do download the latest code from the gitlab server in branch production
    - Edit or create a yaml file in the correct directory
    - Add the file to git (`git add <file name>`)
    - Create a git commit (`git commit -m "<message about what was added>"`)
    - Push the code to gitlab (`git push`)
    - Identify commit hash for use in the control-repo/Puppetfile, run `git show --summary`
    
### Add new hieradata or profile to production puppet deployment

- From a terminal on host with the repositories in $HOME/code/control-repo directory
    - Run `git pull origin production` this is going do download the latest code from the gitlab server in branch production
    - Edit Puppetfile ('vim Puppetfile')
    - Add 'mod' line in the specific location of the file from the module page on the forge.puppet.com 
        - example:  
        ```        mod 'profile',
                 :git    => 'git@agent.puppetlabs.vm:puppet/profile.git',
                 # last commit hash can be found with  `git show --summary`
                 :commit => '<new commit hash from git>'```
    - Add the new version of the file to git with `git add Puppetfile`
    - Create a git commit (`git commit -m "<message about what was added>"`)
    - Push the code to gitlab (`git push`)
- From a terminal on the puppetmaster as root
    - Run `puppet code deploy production --wait`
- If the deploy completes with no errors the new version or module has been installed
    - This will only add a new version/module to the index of modules that can be used.

    