## Guideline for Profile Creation ##

- Profiles should encapsulate a technology.
- Simple wrapper classes for site specifics.
- If params are used any changes in settings should be done though hiera
- May have/require case statement to ensure appropriate configuration. Example:


```
    case $::osfamily {
      'RedHat': { .....
```

- May manage resources for a configuration, ie.
  - packages, users, files, etc.

